# Overskrift 1

Noget brødtekst

## Første underoverskrift

På de kommende linjer skulle der gerne være en liste

* En simpel liste  oprettes med en stjerne efterfulgt af et mellemrum.
* *Kursiv skrift skrives med \*'er omkring ordene uden mellemrum*.
* **Fed skrift fås ved at omslutte ord eller sætninger med 2 stjerner.**
* ***Fed og kursiv skrift fås ved at bruge tre stjerner***.

Direkte links til hjemmesider kan skrives som f.eks. <https://google.com>, ved at indsætte linket mellem
mindre end- og større end-tegn.

I gitlab flavored markdown kan ligninger, som skal stå på en linje skrives på følgende måde, 
med Pythagoras' lærersætning som eksempel $`a^2 + b^2 = c^2`$, 
mens en ligning, som skal stå for sig selv, skrives som

```math
E = m \cdot c^2
```

Samme metode kan bruges til at kodeafsnit ind. Her er et eksempel på lidt python

```python
import math
print("Tallet pi har foelgende vaerdi {}".format(math.pi))
```

I kan se, hvordan filen ser ud ved at besøge denne fil [Jacobs første udgave af en logbog](https://gitlab.com/jacobdebel/logbog.md).

